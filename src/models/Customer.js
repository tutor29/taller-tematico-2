const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
    name: String,
    address: String,
    phone: Number,
    email: String,
    // ticket: {
    //     type: mongoose.Types.ObjectId,
    //     ref: 'Ticket',
    // }
},
    {
        timestamps: true,
        versionKey: false,
    }
);

module.exports = mongoose.model('Customer', customerSchema);