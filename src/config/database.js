const mongoose = require('mongoose');

const mongodbConnection = async function(){
    try{
        const db = await mongoose.connect('mongodb://localhost:27017/restaurant');
        console.log(`Database is conected to ${db.connection.name}`);
    }catch(err){
        console.log(err.message);
    }
}

module.exports = mongodbConnection;