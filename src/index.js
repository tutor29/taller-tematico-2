const express = require('express');
const dotenv = require('dotenv');
const mongodbConnection = require('./config/database');
const routesCustomer = require('./routers/customer.routes')
const routesIndex = require('./routers/index.routes');
const cors = require('cors');
// Llamamos el metodo de conexión a la base de datos
mongodbConnection();


// Instanciamos el servidor de express()
const app = express();


//iniciamos las variables de entorno
dotenv.config({
    path: './.env'
});

// Iniciarmos la configuración de los cors por defecto
app.use(cors());

app.use(express.json());

app.use('/customer', routesCustomer);

const port = process.env.PORT || 4000;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})