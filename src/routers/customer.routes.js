const { Router } = require('express');
const Customer = require('../models/Customer');

const router = Router();

router.get('/', (req, res) => {
    res.send('Parece que todo funciona bien');
});

router.post('/', async (req, res) => {
    const { name, address, email, phone } = req.body;
    const newCustomer = new Customer({
        name,
        address,
        email,
        phone,
    })
    try{
        const savedCustomer = await newCustomer.save();
        res.json(savedCustomer);
    }catch(err){
        res.send(err.message)
    }
});

// router.put();

// router.delete();

module.exports = router;